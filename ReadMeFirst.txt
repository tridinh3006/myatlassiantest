##################
# 1. Description #
##################
	-	The scripts are automated on Atlassians public instance: https://jira.atlassian.com/browse/TST
	-	Using selenium-java-2.4.5.
	- 	Basically, the scripts will be executed with the following flows:
		1. Log in
		2. Create an issue: you can create any kind of issues: Bug, New Feature,...In the script, I created two cases:
		- One is Project: "A Test Project (TST)" and Issue Type: "New Feature" are kept as default when you open "Create Issue" dialog
		- Second: select project "Angry Nerbs" and issue type "Bug"
		3. Verify the new issue in the recent issues list.

#######################
# 2. Script structure # the project is structured with the following folders:
#######################
a. src: this folder contains:
	- automationTest:	the main script to run test case.
	- PageObjects:		define the page objects as: 	CreateIssuePage, DefaultPage, HomePage, LoginPage.
	- Resources:		contain the resource files:
		+ CreateIssue_Info.xlsx:	you can add the information of issue to this file and the script will read from it and input to test site.
		+ TestAttachment.jpg:		the picture will be used as attachment when creating a new issue.
	- Utilities:
		+ Browsers.java:	where you can define the browsers, version to run the test (you need to install web driver for each of them).
		+ CommonFunctions.java:	where the common functions are defined.
		+ Constant.java:	where constant variables are defined.
		+ ExcelUtil.java:	define functions to read excel file.
		+ Setup.java:		define setup and teardown for script.
b. libs: all the external jars files which are used will be added here.

############
# 3. Setup #
############

Precondition:
	- The project was developed on JDK 8u40 and Eclipse Luna SR2. So these prerequisites need to be matched.
	
a. After imported project to eclipse. You need to add external jars to build path:
	- Open project from Eclipse.
	- Righ click on Project > Select Properties > Java build path. Then navigate to Libraries tab and click Add JARs.
	- Browse to "libs" folder in "src" folder and add all jars.

b How to run:
	- Currently, the script is using my account to log in Jira. So assumption you will use this account to verify. They are configured in "Utilities\Setup.java" file
	- Take a look main script: "CreateIssue_TC.java", there are two important parameters you can be changed:
		1. objNewIssue.testDataLine: = 1: it determine which line from excel file will be read to input test data to create issue form.
		2. objNewIssue.mode = 2:  currently, I created two modes for your verification:
			+ mode = 1: project and issue type will be selected from drop down lists. But this mode is not stable due to the issues which I listed in "Issues" section below.
			+ mode = 2: project and issue type will be kept as default when opening "Create Issue" dialog. This mode is more stable than mode 1.
			
#############
# 4. Issues #
#############
There are some issues when using Atlassian's public instance:
	- The site often shows: "Warning: Unresponsive Scripts" dialog and this prevents the scripts run successfully.
	- The site is often "Not Responding" when selecting "Project" and "Issue Type" in drop down list when running script.
	- Worked around: to disable the warning unresponsive scripts, I tried to modify the references of Firefox driver as below:
		+ Set "dom.max_chrome_script_run_time" = 0.
		+ Set "dom.max_script_run_time" = 0.
		Unfortunately, the scripts still be stopped after selecting the values in dropdown lists: Project and Issue Type.
	

		
			