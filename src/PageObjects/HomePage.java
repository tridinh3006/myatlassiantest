package PageObjects;

import org.openqa.selenium.WebElement;

import Utilities.CommonFunctions;
import Utilities.Constant;

public class HomePage {
	public static WebElement UserProfile() {
		return CommonFunctions.getElement(Constant.How.ID, "header-details-user-fullname");
	}
	
	public static WebElement btCreateIssue() {
		return CommonFunctions.getElement(Constant.How.ID, "create_link");
	}
	
	public static void createIssue() {
		btCreateIssue().click();
	}
}
