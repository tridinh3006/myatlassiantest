package PageObjects;



import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opera.core.systems.scope.protos.UmsProtos.Command;

import Utilities.CommonFunctions;
import Utilities.Constant;
import Utilities.ExcelUtils;
import Utilities.Setup;


public class CreateIssuePage {
	
	public static class ISSUE_TYPE {
		public static final String NEWFEATURE = "New Feature";
		public static final String BUG = "Bug";
		public static final String IMPROVEMENT = "Improvement";
		public static final String SUPPORTREQUEST = "Support Request";
		public static final String TASK = "Task";
		public static final String THIRDPARTY_ISSUE = "Third-party issue";		
	}
	
	public static class SECURITY_LEVEL {
		public static final String NONE = "None";
		public static final String LOGGEDIN_USERS = "Logged-in users";
		public static final String REPORTER_DEVELOPERS = "Reporter and developers";
	}
	
	public static class PRIORITY {
		public static final String MINOR = "Minor";
		public static final String BLOCKER = "Blocker";
		public static final String CRITICAL = "Critical";
		public static final String MAJOR = "Major";
		public static final String TRIVIAL = "Trivial";				
	}
	
	public static class COMPONENTS {
		public static final String BACKEND = "Backend ";
		public static final String MOTION_ENGINE = "Motion Engine ";
		public static final String IMAGES = "Images ";
		public static final String UI = "UI ";
		public static final String COMPONENT_2 = "Component 2 ";
	}
	
	public static class AFFECT_VERSIONS {
		public static final String UNRELEASED = "Unreleased Versions" ;
		public static final String VERSION_2 = "Version 2.0";
		public static final String DEV_VERSION = "Dev Version";
		public static final String VERSION_1 = "1.0.1";
	}
	
	public static class FIX_VERSIONS {
		public static final String TEST_VERSION = "Test Version 1" ;
		public static final String VERSION_2 = "Version 2.0";
		public static final String DEV_VERSION = "Dev Version";
	}
	
	public static class VENDOR {
		public static final String BEA = "bea";
		public static final String ORACLE = "Oracle";
		public static final String MICROSOFT = "Microsoft";
		public static final String CISCO = "Cisco";
		public static final String NONE = "None";				
	}
	
	public static class PARENT_ANIMAL {
		public static final String BIRDS = "Birds";
		public static final String FISH = "Fish";
		public static final String INSECTS = "Insects";
		public static final String NONE = "None";
	}
	
	public static class CHILD_ANIMAL {
		public static final String KAGAROO = "Kagaroo";
		public static final String GOALA = "Goala";
		public static final String NONE = "None";
	}
	
	public static class MYVERSION {
		public static final String UNKNOWN = "Unknown";
		public static final String AVERSION = "A version";
		public static final String VERSION_2 = "Version 2.0";
	}
	
	public static WebElement dialogCreateIssue() {
		return CommonFunctions.getElement(Constant.How.ID, "create-issue-dialog");
	}
	
	public static WebElement listItemLink() {
		return CommonFunctions.getElement(Constant.How.CSSSELECTOR, "#all-projects a.aui-list-item-link aui-iconised-link");
	}
	
	
	public static WebElement ddlProject() {
		return CommonFunctions.getElement(Constant.How.ID, "project-field");
	}
	
	public static WebElement pickerProject() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//div[@id='project-single-select']/span");
	}
	
	public static WebElement ddlIssueType() {
		return CommonFunctions.getElement(Constant.How.ID, "issuetype-field");
	}
	
	public static WebElement pickerIssueType() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//div[@id='issuetype-single-select']/span");
	}
	
	public static WebElement txtSummary() {
		return CommonFunctions.getElement(Constant.How.CSSSELECTOR, ".content input[id='summary']");
	}
	
	public static WebElement ddlSecurityLevel() {
		return CommonFunctions.getElement(Constant.How.ID, "security");
	}
	
	public static WebElement ddlPriority() {
		return CommonFunctions.getElement(Constant.How.ID, "priority-field");
	}
	
	public static WebElement pickerPriority() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//div[@id='priority-single-select']/span");
	}
	
		
	public static WebElement pickerDueDate() {
		return CommonFunctions.getElement(Constant.How.CSSSELECTOR, "#duedate-trigger span");
	}
	
	public static WebElement widgetDueDate() {
		return CommonFunctions.getElement(Constant.How.CSSSELECTOR, "div[class='calendar active'] table");
	}
	
	public static WebElement ddlComponents() {
		return CommonFunctions.getElement(Constant.How.ID, "components-textarea");
	}
	
	public static WebElement pickerComponents() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//div[@id='components-multi-select']/span");
	}
	
	public static WebElement ddlVersion() {
		return CommonFunctions.getElement(Constant.How.ID, "versions-textarea");
	}
	
	public static WebElement pickerVersions() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//div[@id='versions-multi-select']/span");
	}
	
	public static WebElement ddlFixVersion() {
		return CommonFunctions.getElement(Constant.How.ID, "fixVersions-textarea");
	}
	
	public static WebElement pickerFixVersions() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//div[@id='fixVersions-multi-select']/span");
	}
	
	public static WebElement txtAssignee() {
		return CommonFunctions.getElement(Constant.How.ID, "assignee-field");
	}
	
	public static WebElement txtEnv() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//textarea[@id='environment']");
	}
	
	public static WebElement txtDesc() {
		return CommonFunctions.getElement(Constant.How.ID, "description");
	}
	
	public static WebElement txtCustom() {
		return CommonFunctions.getElement(Constant.How.CLASSNAME, "text full-width-field wiki-textfield");
	}
	
	public static WebElement pickerVendorDate() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//div[@class='field-group aui-field-datepicker']/span");
	}
	
	public static WebElement txtVendor() {
		return CommonFunctions.getElement(Constant.How.CLASSNAME, "select cf-select");
	}
	
	//find best solution for element Buger options
	
	public static WebElement chboxBugerLettuce() {
		return CommonFunctions.getElement(Constant.How.CSSSELECTOR, "#group .checkbox.checkbox:nth-child(0)");
	}
	
	public static WebElement chboxBugerTomato() {
		return CommonFunctions.getElement(Constant.How.CSSSELECTOR, "#group .checkbox.checkbox:nth-child(1)");
	}
	
	public static WebElement chboxBugerOnion() {
		return CommonFunctions.getElement(Constant.How.CSSSELECTOR, "#group .checkbox.checkbox:nth-child(2)");
	}
	
	public static WebElement chboxBugerChilli() {
		return CommonFunctions.getElement(Constant.How.CSSSELECTOR, "#group .checkbox.checkbox:nth-child(3)");
	}
	
	public static WebElement btAttachment() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//label[@class='issue-drop-zone__button aui-button']");
	}
	
	public static WebElement txtTimeTracking() {
		return CommonFunctions.getElement(Constant.How.ID, "timetracking");
	}
	
	public static WebElement dropListParentAnimal() {
		return CommonFunctions.getElement(Constant.How.CLASSNAME, "select cascadingselect-parent");
	}
	
	public static WebElement dropListChildAnimal() {
		return CommonFunctions.getElement(Constant.How.CLASSNAME, "select cascadingselect-child");
	}
	
	public static WebElement txtTester() {
		return CommonFunctions.getElement(Constant.How.CLASSNAME, "text long-field userpickerfield");
	}
	
	public static WebElement fieldVersionPicker() {
		return CommonFunctions.getElement(Constant.How.CLASSNAME, "js-default-checkboxmultiselect select");
	}
	
	public static WebElement txtLabels() {
		return CommonFunctions.getElement(Constant.How.ID, "labels-textarea");
	}
	
	public static WebElement txtStoryPoint() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//input[@class='text long-field']");
	}
	
	
	public static WebElement ddlEpicLink() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//div[@class='field-group aui-field-labelpicker']/input");
	}
	
	public static WebElement pickerEpicLink() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//div[@class='field-group aui-field-labelpicker']/span");
	}
	
	//find best solution for field "Regular Expression"
	public static WebElement txtRegx() {
		return CommonFunctions.getElement(Constant.How.CSSSELECTOR, "input.text [type=text][maxlength=254]");
	}
	
	public static WebElement btCreateIssueSubmit() {
		return CommonFunctions.getElement(Constant.How.ID, "create-issue-submit");
	}
	
	public static WebElement issuesMenu() {
		return CommonFunctions.getElement(Constant.How.XPATH, "//a[@id='find-link'][@title='Search for issues and view recent issues']");
	}
	
	public static class ObjNewIssue {
		public String issueType;
		public String securityLevel;
		public String priority;
		public String components;
		public String affectVersion;
		public String fixVersion;
		public String vendor;
		public String parentAnimal;
		public String childAnimal;
		public String myVersion;
		public int testDataLine;
		public String mode;
	}
	
	public static void selectBugerOption(String option) {
		switch(option) {
			case "Lecttuce":
				chboxBugerLettuce().click();
				break;
			case "Tomato":
				chboxBugerTomato().click();
				break;
			case "Onion":
				chboxBugerOnion().click();
				break;
			case "Chilli":
				chboxBugerChilli().click();
		}
	}
	
	public static void createIssue(ObjNewIssue objNewIssue) throws Exception {
		WebDriverWait wait = new WebDriverWait(Setup._webdriver, 10);
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Sheet1");
		
		String project = ExcelUtils.getCellData(objNewIssue.testDataLine, 0);
		String textsummary = CommonFunctions.summaryGenerator(ExcelUtils.getCellData(objNewIssue.testDataLine, 1));	
		String assignee = ExcelUtils.getCellData(objNewIssue.testDataLine, 2);
		String environment = ExcelUtils.getCellData(objNewIssue.testDataLine, 3);
		String desc = ExcelUtils.getCellData(objNewIssue.testDataLine, 4);
		String randomText = ExcelUtils.getCellData(objNewIssue.testDataLine, 5);
		String bugerOptions = ExcelUtils.getCellData(objNewIssue.testDataLine, 6);
		String orgEstimate = ExcelUtils.getCellData(objNewIssue.testDataLine, 7);
		String tester = ExcelUtils.getCellData(objNewIssue.testDataLine, 8);
		String labels = ExcelUtils.getCellData(objNewIssue.testDataLine, 9);
		String storyPoint = ExcelUtils.getCellData(objNewIssue.testDataLine, 10);
		String sprint = ExcelUtils.getCellData(objNewIssue.testDataLine, 11);
		String epic = ExcelUtils.getCellData(objNewIssue.testDataLine, 12);
		String regx = ExcelUtils.getCellData(objNewIssue.testDataLine, 13);
		
		
		HomePage.btCreateIssue().click();
		CommonFunctions.iWait(20);
		
		Assert.assertNotNull(dialogCreateIssue());		
		
		if(objNewIssue.mode == "1") {
			//Select Project
			CommonFunctions.selectDropdowListValue(ddlProject(), pickerProject(), project, true);	
			Thread.sleep(5000);
			//Select Issue Type
			CommonFunctions.selectDropdowListValue(ddlIssueType(), pickerIssueType(), objNewIssue.issueType, true);		
			
			CommonFunctions.iWait(20);		
			CommonFunctions.waitUntilElementVisible(txtSummary());
		}		
		
		//Input Summary
		txtSummary().sendKeys(textsummary);
		
		//Input Security Level
		ddlSecurityLevel().sendKeys(objNewIssue.securityLevel);
		
		//Select Priority
		CommonFunctions.selectDropdowListValue(ddlPriority(), pickerPriority(), objNewIssue.priority, true);
		
		//If Due Date Picker is visible, select current date
		if(pickerDueDate()!=null) {
			CommonFunctions.selectCurrentDate(pickerDueDate(), true);
		}
		
		//Select Component
		CommonFunctions.selectDropdowListValue(ddlComponents(),pickerComponents(), objNewIssue.components, true);	
		
		//Select Affect Version
		CommonFunctions.selectDropdowListValue(ddlVersion(), pickerVersions(), objNewIssue.affectVersion, true);
		
		
		CommonFunctions.scrollElementIntoView(pickerVersions());		
		//Input Environment
		txtEnv().sendKeys(environment);
		
		//Input Description
		txtDesc().sendKeys(desc);
		
		CommonFunctions.scrollElementIntoView(txtDesc());		
		//Attache a screenshot
		btAttachment().click();		
		CommonFunctions.uploadFile(CommonFunctions.getPathUploadFile(Constant.AttachmentPath));
		Thread.sleep(1000);
		
		//Input Org Estimate
		txtTimeTracking().sendKeys(orgEstimate);
		
		CommonFunctions.scrollElementIntoView(txtLabels());
		txtLabels().sendKeys(labels);
		if(txtTester() != null) {
			txtTester().sendKeys(tester);	
		}		
				
		//Submit Issue
		btCreateIssueSubmit().click();
		
		String titleIssue = project + " " + textsummary;
		verifySubittedIssue(titleIssue);
	}
	
	public static void verifySubittedIssue(String titleIssue) {		
		CommonFunctions.waitUntilElementClickAble(issuesMenu());
		issuesMenu().click();
		WebElement recentIssue = CommonFunctions.getElement(Constant.How.XPATH, "//ul[@id='issue_history_main']/a[@title='" + titleIssue + "']");
		Assert.assertNotNull(recentIssue);
	}
}
