package PageObjects;

import org.junit.Assert; 
import Utilities.CommonFunctions;
import Utilities.Constant;



import org.openqa.selenium.WebElement;

public class LoginPage {
	
	public static WebElement btSignIn() {
		return CommonFunctions.getElement(Constant.How.ID, "login-submit");
	}
	
	public static WebElement txtUserName() {
		return CommonFunctions.getElement(Constant.How.ID, "username");
	}
	
	public static WebElement txtPassWord() {
		return CommonFunctions.getElement(Constant.How.ID, "password");
	}
	
	public static WebElement chxStaySignedIn() {
		return CommonFunctions.getElement(Constant.How.ID, "rememberMe");			
	}
	
	public static void signIn(String userName, String passWord, boolean isRememberMe) {		
		DefaultPage.logIn();
		txtUserName().sendKeys(userName);
		txtPassWord().sendKeys(passWord);
		if((isRememberMe && !chxStaySignedIn().isSelected()) ||
		   (!isRememberMe && chxStaySignedIn().isSelected())) {
			chxStaySignedIn().click();
		}
		
		btSignIn().click();		
		Assert.assertNotNull(HomePage.UserProfile());
	}
	
}
