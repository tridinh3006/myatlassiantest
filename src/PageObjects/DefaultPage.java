package PageObjects;

import org.openqa.selenium.WebElement;

import Utilities.CommonFunctions;
import Utilities.Constant;

public class DefaultPage {
	public static WebElement btLogin() {
		return CommonFunctions.getElement(Constant.How.LINKTEXT, "Log In");
	}
	
	public static void logIn() {
		btLogin().click();
		CommonFunctions.iWait(20);
	}
}
