package automationTest;

import PageObjects.CreateIssuePage;
import Utilities.Setup;

public class CreateIssue_TC {

	public static void main(String[] args) throws Exception {
		Setup.SetUp();
		
		CreateIssuePage.ObjNewIssue objNewIssue = new CreateIssuePage.ObjNewIssue();
		objNewIssue.testDataLine = 1;
		objNewIssue.mode = "2";
		
		if(objNewIssue.mode == "1") {
			objNewIssue.components = CreateIssuePage.COMPONENTS.BACKEND;
			objNewIssue.affectVersion = CreateIssuePage.AFFECT_VERSIONS.VERSION_1;
		} else {
			objNewIssue.components = CreateIssuePage.COMPONENTS.COMPONENT_2;
			objNewIssue.affectVersion = CreateIssuePage.AFFECT_VERSIONS.VERSION_2;
		}
		
		objNewIssue.issueType = CreateIssuePage.ISSUE_TYPE.BUG;
		objNewIssue.securityLevel = CreateIssuePage.SECURITY_LEVEL.REPORTER_DEVELOPERS;
		objNewIssue.priority = CreateIssuePage.PRIORITY.MAJOR;
				
		objNewIssue.vendor = CreateIssuePage.VENDOR.CISCO;
		
		CreateIssuePage.createIssue(objNewIssue);
		
		Setup.TearDown();

	}

}