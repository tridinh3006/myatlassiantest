package Utilities;

public class Browsers {
	public class Chrome {
		public static final String Name = "chrome";
		public static final String Version27 = "27";
		public static final String DefaultVersion = "27";
	}
	
	public class Firefox {
		public static final String Name = "firefox";
		public static final String Version27 = "27";
		public static final String DefaultVersion = "27";
	}
	
	public class InternetExplorer {
		public static final String Name = "internet explorer";
		public static final String Version7 = "7";
		public static final String Version8 = "8";
		public static final String Version9 = "9";
		public static final String Version10 = "10";
		public static final String Version11 = "11";
		
	}
}
