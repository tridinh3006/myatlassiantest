package Utilities;

import java.io.File;
import java.io.FileInputStream;


import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils{
	private static XSSFSheet ExcelWSheet;		 
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	
	public static void setExcelFile(String Path,String SheetName) throws Exception {			 
		try {
			String currentPath = new File(".").getCanonicalPath();			
			FileInputStream ExcelFile = new FileInputStream(currentPath + Path);
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);

		} catch (Exception e){
			throw (e);
		}
	}
	
	public static String getCellData(int RowNum, int ColNum) throws Exception{			 
		try {
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = Cell.getStringCellValue();
			return CellData;
		} catch (Exception e){
			return"";
		}
    }
	
	
}
