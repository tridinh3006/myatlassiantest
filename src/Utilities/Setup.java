package Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;

import PageObjects.LoginPage;


public class Setup {
	//The WEB DRIVER
	public static WebDriver _webdriver;
	
	public static String _testsite = Constant.CoBrand.JIARSANDBOX;
	
	//Default value for local testing
	public static String _browserName = Browsers.Firefox.Name;
	
	public static String _userName = "tridinh3006@gmail.com";
	public static String _pass = "nhatminh267";
	
	public static void initWebDriver () {
		switch(_browserName) {
			case Browsers.Firefox.Name:
				_webdriver = new FirefoxDriver();
				break;
			case Browsers.Chrome.Name:
				_webdriver = new ChromeDriver();
				break;
			case Browsers.InternetExplorer.Name:
				_webdriver = new InternetExplorerDriver();
		}
	}
	
	public static void SetUp() {
		initWebDriver();
		_webdriver.manage().window().maximize();
		CommonFunctions.gotoUrl(_testsite);
		LoginPage.signIn(_userName, _pass, true);
	}
	
	public static void TearDown() {
		//We can define logOut function at here
		_webdriver.close();
	}
	
}
